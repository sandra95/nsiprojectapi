var express=require('express'); 
var app=express();
var port=1432;
var bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({ extended: false }))

app.use(bodyParser.json())
var CommentController=require('./Controller/CommentController')();
app.use("/api",CommentController);  
var conn=require("./Connection/Connection.js")();
conn.connect(function (err)
{ 
 if (err) throw err;             
     console.log("Connected!");

 });

app.listen(port, function()
{
    var datetime=new Date();
    var message="Servers id running on Port: -" +port+"started at:-"+datetime;
    console.log(message);
});
