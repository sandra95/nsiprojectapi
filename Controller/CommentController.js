//ucitavanje spoljasnjih modula
var express=require('express');//koji framework kreira api
var router=express.Router(); //ruter ya kreiranje rute
var sql=require("mysql");// server
var conn=require("../Connection/Connection.js")();// konekciju

var routes=function()
{
    router.route('/comment')
        .get(function(req, res){         
             conn.query("Select * from comment", function (err,result) {
               if (err) throw err;             
               res.json(result);
             });             
        }); 
        router.route('/users')
        .get(function(req, res){         
             conn.query("Select * from users", function (err,result) {
               if (err) throw err;             
               res.json(result);
             });             
        }); 
        router.route('/comment/test/:c')
        .get(function(req, res){         
             conn.query("Select * from comment where city='"+req.params.c+"'", function (err,result) {
               if (err) throw err;             
               res.json(result);
             });             
        });       
        router.route('/comment')
          .post( function(req,res){
            var sql = "INSERT INTO comment (user, text, positive, negative, city) VALUES ('"+req.body.user+"','"+ req.body.text+"',"+req.body.positive+","+ req.body.negative+",'"+ req.body.city+"');"
            conn.query(sql, function (err, result) {
                if (err) throw err;             
                 console.log("1 record inserted, ID: " + req.body.user);
                 res="inserted";

            });
               
        });
        router.route('/users')
          .post( function(req,res){
            var sql = "INSERT INTO users (name, email) VALUES ('"+req.body.name+"','"+ req.body.email+"'"+");"
            conn.query(sql, function (err, result) {
                if (err) throw err;             
                 console.log("1 record inserted, ID: " + req.body.user);
                 res="inserted";

            });
               
        });
        router.route('/comment/:id')
        .put( function(req,res){
            console.log("this is id "+req.body.id_comment);
          var sql = "UPDATE  comment SET  positive="+req.body.positive+", negative="+req.body.negative+" where id_comment="+req.body.id_comment;
          conn.query(sql, function (err, result) {
              if (err) throw err;             
               console.log("1 record updated, ID: " + req.body.user);
               res="updated";
          });
      });
      router.route('/comment/change/:id')
        .put( function(req,res){
            console.log("this is id "+req.body.id_comment);
          var sql = "UPDATE  comment SET  text='"+req.body.text+"' where id_comment="+req.body.id_comment;
          conn.query(sql, function (err, result) {
              if (err) throw err;             
               console.log("1 record updated, ID: " + req.body.user);
               res="updated";
          });
      });
      router.route('/comment/:id')
      .delete( function(req,res){
        var sql = "DELETE from  comment  where id_comment="+req.params.id;
        conn.query(sql, function (err, result) {
            if (err) throw err;             
             console.log("1 record deleted, ID: " + req.body.user);
             res="updated";
        });
    });
    router.route('/comment/:id')
    .get( function(req,res){
     var sql = "Select * from  comment  where id_comment="+req.params.id;
      conn.query(sql, function (err,result) {
        if (err) throw err;             
        res.json(result);
      });
  });
   router.route('/comment/text/:id')
    .get( function(req,res){
     var sql = "Select * from  comment  where user='"+req.params.id+"'";
      conn.query(sql, function (err,result) {
        if (err) throw err;             
        res.json(result);
      });
  });
        return router;  
};
module.exports=routes;